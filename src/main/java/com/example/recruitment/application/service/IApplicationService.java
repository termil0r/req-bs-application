package com.example.recruitment.application.service;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by t0meck on 29.05.2016.
 */
public interface IApplicationService<T extends ApplicationEntity> {

	T create(T application);
	T verify(T application);
	T accept(T application);
	T publish(T application);
	T delete(T application);
	T reject(T application);
	T update(T application);
	List<T> list(String name, ApplicationStatus status, Pageable pageable);
}
