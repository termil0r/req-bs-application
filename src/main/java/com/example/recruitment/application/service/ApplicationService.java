package com.example.recruitment.application.service;

import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by t0meck on 29.05.2016.
 */
@Service
public class ApplicationService implements IApplicationService<ApplicationEntity> {

	@Autowired
	private ApplicationDao dao;

	@Override
	public ApplicationEntity create(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.CREATED);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity verify(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.VERIFIED);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity accept(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.ACCEPTED);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity publish(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.PUBLISHED);
		application.setNumber(dao.getHighestNumberValue() + 1L);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity delete(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.DELETED);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity reject(@NotNull ApplicationEntity application) {
		application.setStatus(ApplicationStatus.REJECTED);
		return dao.save(application);
	}

	@Override
	public ApplicationEntity update(@NotNull ApplicationEntity application) {
		if (application.getStatus() == ApplicationStatus.CREATED || application.getStatus() == ApplicationStatus.VERIFIED) {
			return dao.save(application);
		}
		return null;
	}

	@Override
	public List<ApplicationEntity> list(String name, ApplicationStatus status, Pageable pageRequest) {
		List<ApplicationEntity> result;

		if (name == null && status == null) {
			// return all
			Page<ApplicationEntity> page = dao.findAll(pageRequest);
			result =page.getContent();
		} else if (name != null && status == null) {
			result = dao.findByNameIgnoreCaseContaining(name, pageRequest);
		} else if (name == null && status != null) {
			result = dao.findByStatus(status, pageRequest);
		} else {
			result = dao.findByNameIgnoreCaseContainingAndStatus(name, status, pageRequest);
		}
		return result;
	}

}
