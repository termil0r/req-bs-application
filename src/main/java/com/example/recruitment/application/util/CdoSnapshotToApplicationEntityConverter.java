package com.example.recruitment.application.util;

import com.example.recruitment.application.model.ApplicationEntity;
import com.google.gson.Gson;
import org.javers.core.Javers;
import org.javers.core.json.JsonConverter;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.core.metamodel.object.CdoSnapshotState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.lang.reflect.Field;

/**
 * Created by t0meck on 28.05.2016.
 */
@Component
@Converter(autoApply = true)
public class CdoSnapshotToApplicationEntityConverter implements AttributeConverter<CdoSnapshot, ApplicationEntity> {

	@Autowired
	private Javers javers;

	@Override
	public ApplicationEntity convertToDatabaseColumn(CdoSnapshot snapshot) {
		return convertToEntityByGson(snapshot.getState());
	}

	private ApplicationEntity convertToEntityByReflection(CdoSnapshotState state) {
		ApplicationEntity entity = new ApplicationEntity();
		for (Field field : ApplicationEntity.class.getDeclaredFields()) {
			try {
				field.setAccessible(true);
				Object value = state.getPropertyValue(field.getName());
				field.set(entity, value);
				field.setAccessible(false);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return entity;
	}

	private ApplicationEntity convertToEntityByGson(CdoSnapshotState state) {
		JsonConverter gson = javers.getJsonConverter();
		String json = gson.toJson(state);
		return gson.fromJson(json, ApplicationEntity.class);
	}

	@Override
	public CdoSnapshot convertToEntityAttribute(ApplicationEntity entity) {
		throw new UnsupportedOperationException();
	}
}
