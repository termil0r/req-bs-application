package com.example.recruitment.application.util;

import com.example.recruitment.application.model.ApplicationStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by t0meck on 28.05.2016.
 */
@Converter(autoApply = true)
public class ApplicationStatusConverter implements AttributeConverter<ApplicationStatus, String> {

	@Override
	public String convertToDatabaseColumn(ApplicationStatus attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getDatabaseValue();
	}

	@Override
	public ApplicationStatus convertToEntityAttribute(final String dbData) {
		if (dbData == null) {
			return null;
		}

		String trimmed = dbData.trim();
		Optional<ApplicationStatus> statusOptional = Arrays.stream(ApplicationStatus.values()).filter(s -> s.getDatabaseValue().equals(trimmed)).findFirst();

		if (statusOptional.isPresent()) {
			return statusOptional.get();
		} else {
			throw new IllegalArgumentException("Cannot convert from '"+dbData+"' to "+ApplicationStatus.class.getSimpleName());
		}
	}
}
