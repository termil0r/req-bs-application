package com.example.recruitment.application.model;

import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.util.ApplicationStatusConverter;
import com.example.recruitment.application.validation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@ApplicationNumberAssigned
@DeletionCauseGiven
@ApplicationStatusValidForEntityModificationValidation
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "APPLICATION")
@SequenceGenerator(name = "defaultEntityGenerator", sequenceName = "APPLICATION", allocationSize = 1)
public class ApplicationEntity extends AEntity {

	/**
	 * Given when application is published
	 */
	private Long number;

	@NotNull
	private String name;

	@NotNull
	private String description;

	private String deletionCause;

	// Converter cannot be applied to fields annotated with @Enumerated
	// @Enumerated(EnumType.STRING)
	@Convert(converter = ApplicationStatusConverter.class)
	@NotNull
	private ApplicationStatus status;

	public void setStatus(ApplicationStatus status) {
		if (this.status == status) {
			return;
		}

		if (this.status == null) {
			this.status = status;
			return;
		}

		if (status != null) {
			if (SimpleApplicationStatusSequenceValidator.isValidTransition(this.status, status)) {
				this.status = status;
				return;
			}
		}

		throw new UnsupportedOperationException();
	}

	public void clearStatus() {
		this.status = null;
	}

}
