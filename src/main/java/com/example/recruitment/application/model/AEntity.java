package com.example.recruitment.application.model;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by t0meck on 26.05.2016.
 */
@Data
@MappedSuperclass
public abstract class AEntity implements IEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "defaultEntityGenerator")
	private Long id;

}
