package com.example.recruitment.application.model;

import lombok.Getter;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by t0meck on 27.05.2016.
 */
public enum ApplicationStatus {

	CREATED("Created", "C"),
	VERIFIED("Verified", "V"),
	ACCEPTED("Accepted", "A"),
	PUBLISHED("Published", "P"),
	DELETED("Deleted", "D"),
	REJECTED("Rejected", "R");

	@Getter private String screenName;
	@Getter private String databaseValue;

	ApplicationStatus(String screenName, String databaseValue) {
		this.screenName = screenName;
		this.databaseValue = databaseValue;
	}
}
