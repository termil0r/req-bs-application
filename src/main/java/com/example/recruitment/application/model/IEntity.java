package com.example.recruitment.application.model;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by t0meck on 26.05.2016.
 */
@Entity
public interface IEntity extends Serializable {

	@Id
	Long getId();

	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "defaultEntityGenerator")
	void setId(Long id);
}
