package com.example.recruitment.application.dal;

import com.example.recruitment.application.model.IEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created by t0meck on 28.05.2016.
 */
@Repository
public interface IEntityDao<E extends IEntity, T_ID extends Serializable> extends PagingAndSortingRepository<E, T_ID> {
}
