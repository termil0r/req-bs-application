package com.example.recruitment.application.dal;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by t0meck on 27.05.2016.
 */
@Repository
@JaversSpringDataAuditable
public interface ApplicationDao extends IEntityDao<ApplicationEntity, Long> {

	@Query("SELECT COALESCE(max(a.number), 0) FROM ApplicationEntity a")
	Long getHighestNumberValue();

	List<ApplicationEntity> findByNameIgnoreCaseContaining(String name, Pageable pageRequest);

	List<ApplicationEntity> findByStatus(ApplicationStatus status, Pageable pageRequest);

	// Table which shows available keywords for constructing queries by method name
	// http://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
	List<ApplicationEntity> findByNameIgnoreCaseContainingAndStatus(String name, ApplicationStatus status, Pageable pageRequest);
}
