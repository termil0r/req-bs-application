package com.example.recruitment.application;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * This class is workaround to allow manual injection of beans into Hibernate validators.
 * Normally they do not have access to spring container thus all @Autowired fields in such
 * validators are null when Hibernate invoke validation just before persiting them in db.
 */
@Component
public class ApplicationContextProvider implements ApplicationContextAware {

	private static  ApplicationContext CONTEXT;

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		ApplicationContextProvider.CONTEXT = context;
	}

	public static <T> T getBean(Class clazz) {
		return (T) ApplicationContextProvider.CONTEXT.getBean(clazz);
	}

	public static <T> T getBean(String qualifier, Class clazz) {
		return (T) ApplicationContextProvider.CONTEXT.getBean(qualifier , clazz);
	}
}
