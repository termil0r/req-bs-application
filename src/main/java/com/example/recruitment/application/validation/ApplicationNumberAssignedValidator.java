package com.example.recruitment.application.validation;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by t0meck on 29.05.2016.
 */
public class ApplicationNumberAssignedValidator implements ConstraintValidator<ApplicationNumberAssigned, ApplicationEntity> {

	@Override
	public void initialize(ApplicationNumberAssigned constraintAnnotation) {
	}

	@Override
	public boolean isValid(ApplicationEntity value, ConstraintValidatorContext context) {
		if (value == null || value.getStatus() != ApplicationStatus.PUBLISHED) {
			return true;
		}

		return value.getNumber() != null;
	}
}
