package com.example.recruitment.application.validation;

import com.example.recruitment.application.model.ApplicationStatus;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import static com.example.recruitment.application.model.ApplicationStatus.*;

/**
 * Created by t0meck on 30.05.2016.
 */
public class SimpleApplicationStatusSequenceValidator {

	private static Map<ApplicationStatus, EnumSet<ApplicationStatus>> previous = new HashMap<>();
	private static Map<ApplicationStatus, EnumSet<ApplicationStatus>> next = new HashMap<>();

	static {
		setStates(CREATED, getEnumSet(), getEnumSet(VERIFIED, DELETED));
		setStates(VERIFIED, getEnumSet(CREATED), getEnumSet(REJECTED, ACCEPTED));
		setStates(ACCEPTED, getEnumSet(VERIFIED), getEnumSet(PUBLISHED, REJECTED));
		setStates(PUBLISHED, getEnumSet(ACCEPTED), getEnumSet());
		setStates(DELETED, getEnumSet(CREATED), getEnumSet());
		setStates(REJECTED, getEnumSet(VERIFIED, ACCEPTED), getEnumSet());
	}

	private static EnumSet<ApplicationStatus> getEnumSet(ApplicationStatus... states) {
		EnumSet result = EnumSet.noneOf(ApplicationStatus.class);
		result.addAll(Arrays.asList(states));
		return result;
	}

	private static void setStates(ApplicationStatus status, EnumSet<ApplicationStatus> previousSet, EnumSet<ApplicationStatus> nextSet) {
		previous.put(status, previousSet);
		next.put(status, nextSet);
	}

	public static EnumSet<ApplicationStatus> getPreviousStatuses(ApplicationStatus status) {
		return previous.get(status);
	}

	public static EnumSet<ApplicationStatus> getNextStatuses(ApplicationStatus status) {
		return next.get(status);
	}

	public static boolean isValidTransition(ApplicationStatus oldStatus, ApplicationStatus newStatus) {
		if (oldStatus == newStatus) {
			return true;
		}

		if (oldStatus == null && newStatus != null) {
			return true;
		}

		if (oldStatus != null && newStatus == null) {
			return false;
		}

		if (oldStatus != null && newStatus != null) {
			return getNextStatuses(oldStatus).contains(newStatus) && getPreviousStatuses(newStatus).contains(oldStatus);
		}
		return false;
	}
}
