package com.example.recruitment.application.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by t0meck on 29.05.2016.
 */
@Target( { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ApplicationStatusValidForEntityModificationValidator.class)
public @interface ApplicationStatusValidForEntityModificationValidation {

	String message() default "{com.example.recruitment.application.validation.applicationstatusvalidforentitymodificationvalidation}";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
}
