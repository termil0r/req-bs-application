package com.example.recruitment.application.validation;

import com.example.recruitment.application.ApplicationContextProvider;
import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.javers.core.diff.changetype.ValueChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by t0meck on 29.05.2016.
 */
@Component
public class ApplicationStatusValidForEntityModificationValidator implements ConstraintValidator<ApplicationStatusValidForEntityModificationValidation, ApplicationEntity> {

	@Autowired
	private Javers javers;

	@Autowired
	private ApplicationDao dao;

	@Override
	public void initialize(ApplicationStatusValidForEntityModificationValidation constraintAnnotation) {
		if (dao == null) {
			dao = ApplicationContextProvider.getBean(ApplicationDao.class);
		}

		if (javers == null) {
			javers = ApplicationContextProvider.getBean(Javers.class);
		}
	}

	@Override
	public boolean isValid(ApplicationEntity newVersion, ConstraintValidatorContext context) {
		if (newVersion == null || newVersion.getId() == null) {
			return true;
		}

		ApplicationEntity oldVersion = dao.findOne(newVersion.getId());

		if (oldVersion == null) {
			return true;
		}
		Diff compareResult = javers.compare(oldVersion, newVersion);

		if ((oldVersion.getStatus() == ApplicationStatus.CREATED || oldVersion.getStatus() == ApplicationStatus.VERIFIED) &&
				(newVersion.getStatus() == ApplicationStatus.CREATED || newVersion.getStatus() == ApplicationStatus.VERIFIED)) {
			return true;
		}

		// if there was update in invalid state
		if (compareResult.hasChanges()) {
			List<String> changedProperties = getModifiedProperties(compareResult.getChanges());

			// if any property except status was changed
			if (changedProperties.size() > 0) {
				// if it was changed to deleted && also given delete cause
				// or if it was change to published and assigned number
				if (changedProperties.size() == 1) {
					if (newVersion.getStatus() == ApplicationStatus.PUBLISHED) {
						return changedProperties.contains("number");
					} else if (newVersion.getStatus() == ApplicationStatus.DELETED || newVersion.getStatus() == ApplicationStatus.REJECTED) {
						return changedProperties.contains("deletionCause");
					}
				}
				return false;
			}
			return true;
		}
		return true;
	}

	private List<String> getModifiedProperties(List<Change> changeList) {
		List<String> result = new ArrayList<>();
		for (Change change : changeList) {
			ValueChange valueChange = (ValueChange) change;
			if (!"status".equals(valueChange.getPropertyName())) {
				result.add(valueChange.getPropertyName());
			}
		}

		return result;
	}
}
