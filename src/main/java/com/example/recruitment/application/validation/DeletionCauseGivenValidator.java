package com.example.recruitment.application.validation;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.google.common.base.Strings;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by t0meck on 29.05.2016.
 */
@Component
public class DeletionCauseGivenValidator implements ConstraintValidator<DeletionCauseGiven, ApplicationEntity> {
	@Override
	public void initialize(DeletionCauseGiven constraintAnnotation) {
	}

	@Override
	public boolean isValid(ApplicationEntity value, ConstraintValidatorContext context) {
		if (value == null || value.getStatus() != ApplicationStatus.DELETED && value.getStatus() != ApplicationStatus.REJECTED) {
			return true;
		}

		String cause = value.getDeletionCause();
		if (cause == null) {
			return false;
		}

		cause = StringUtils.trimAllWhitespace(cause);

		return !Strings.isNullOrEmpty(cause);
	}
}
