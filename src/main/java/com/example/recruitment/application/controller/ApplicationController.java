package com.example.recruitment.application.controller;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.example.recruitment.application.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by t0meck on 29.05.2016.
 */
@RequestMapping("/application")
@RestController
public class ApplicationController {

	@Autowired
	ApplicationService service;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public
	@ResponseBody
	Object create(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.create(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/verify", method = RequestMethod.POST)
	public
	@ResponseBody
	Object verify(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.verify(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/accept", method = RequestMethod.POST)
	public
	@ResponseBody
	Object accept(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.accept(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/publish", method = RequestMethod.POST)
	public
	@ResponseBody
	Object publish(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.publish(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/reject", method = RequestMethod.POST)
	public
	@ResponseBody
	Object reject(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.reject(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public
	@ResponseBody
	Object delete(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.delete(application);
		}
		return errors.getAllErrors();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public
	@ResponseBody
	Object update(@RequestBody @Valid ApplicationEntity application, BindingResult errors) {
		if (!errors.hasErrors()) {
			return service.update(application);
		}
		return errors.getAllErrors();
	}

	// Any of the following will work
	// curl -i GET http://localhost:8080/list
	// curl -i GET http://localhost:8080/list -d "name=eNtIt"
	// curl -i GET http://localhost:8080/list -d "status=ACCEPTED"
	// curl -i GET http://localhost:8080/list -d "name=10" -d "status=PUBLISHED"
	// curl -i GET http://localhost:8080/list -d "page=1" -d "size=5" -d "name=eNt" -d "status=DELETED"
	@RequestMapping("/list")
	public List<ApplicationEntity> list(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "status", required = false) ApplicationStatus status, @PageableDefault(size = 10, page = 0) Pageable pageRequest) {
		return service.list(name, status, pageRequest);
	}
}
