package com.example.recruitment.application.service;

import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.example.recruitment.application.util.ApplicationEntityBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by t0meck on 29.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceTest {

	@InjectMocks
	private IApplicationService testee = new ApplicationService();

	@Mock
	private ApplicationDao dao;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testIfCreateWillChangeStatusToCreated() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.create(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.CREATED);
	}

	@Test
	public void testIfVerifyWillChangeStatusToVerified() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.verify(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.VERIFIED);
	}

	@Test
	public void testIfAcceptWillChangeStatusToAccepted() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.accept(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.ACCEPTED);
	}

	@Test
	public void testIfPublishWillChangeStatusToPublished() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.publish(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.PUBLISHED);
	}

	@Test
	public void testIfDeleteWillChangeStatusToDeleted() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.delete(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.DELETED);
	}

	@Test
	public void testIfRejectWillChangeStatusToRejected() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.reject(entity);

		// then
		assertThat(entity.getStatus()).isEqualTo(ApplicationStatus.REJECTED);
	}

	@Test
	public void testIfUpdateWillNotChangeStatus() {
		// given
		exception.expect(UnsupportedOperationException.class);
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();
		List<ApplicationStatus> statuses = Arrays.asList(ApplicationStatus.values());

		// when
		for(ApplicationStatus status : statuses) {
			entity.setStatus(status);
			testee.update(entity);

			// then
		}
	}

	@Test
	public void testIfCreateWillCallSaveOnDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();

		// when
		testee.create(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfVerifyWillCallSaveDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();

		// when
		testee.verify(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfAcceptWillCallSaveDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.accept(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfPublishWillCallSaveDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.publish(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfDeleteWillCallSaveDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.delete(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfRejectWillCallSaveDao() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(null).build();

		// when
		testee.reject(entity);

		// then
		verify(dao, atLeastOnce()).save(entity);
	}

	@Test
	public void testIfUpdateWillThrowUnsupportedOperationExceptionOnNotValidStatusAndNotCallDao() {
		// given
		exception.expect(UnsupportedOperationException.class);
		List<ApplicationStatus> validStatuses = Arrays.asList(ApplicationStatus.CREATED, ApplicationStatus.VERIFIED);
		List<ApplicationStatus> statuses = Arrays.asList(ApplicationStatus.values()).stream().filter(s -> !validStatuses.contains(s)).collect(Collectors.toList());
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();
		ApplicationEntity result = null;
		when(dao.save(entity)).thenReturn(entity);

		// when
		for(ApplicationStatus status : statuses) {
			result = entity;
			result = testUpdate(entity, status);

			// then
			assertThat(result).isNull();
			verify(dao, times(0)).save(entity);
		}
	}

	@Test
	public void testIfUpdateWillCallDaoOnValidStatus() {
		// given
		List<ApplicationStatus> statuses = Arrays.asList(ApplicationStatus.CREATED, ApplicationStatus.VERIFIED);
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();

		// when
		for(ApplicationStatus status : statuses) {
			testUpdate(entity, status);

			// then
			verify(dao, atLeastOnce()).save(entity);
		}
	}

	@Test
	public void testIfCreateWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.create(null);

		// then
	}

	@Test
	public void testIfVerifyWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.verify(null);

		// then
	}

	@Test
	public void testIfAcceptWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.accept(null);

		// then
	}

	@Test
	public void testIfPublishWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.publish(null);

		// then
	}

	@Test
	public void testIfDeleteWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.delete(null);

		// then
	}

	@Test
	public void testIfRejectWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.reject(null);

		// then
	}

	@Test
	public void testIfUpdateWillCrashWhenNullPassed() {
		// given
		exception.expect(NullPointerException.class);

		// when
		testee.update(null);

		// then
	}

	private ApplicationEntity testUpdate(ApplicationEntity entity, ApplicationStatus status) {
		entity.setStatus(status);
		return testee.update(entity);
	}
}
