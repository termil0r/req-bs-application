package com.example.recruitment.application.controller;

import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.example.recruitment.application.service.ApplicationService;
import com.example.recruitment.application.util.ApplicationEntityBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Created by t0meck on 29.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationControllerTest {

	@InjectMocks
	private ApplicationController testee = new ApplicationController();

	@Mock
	private ApplicationService service;

	@Mock
	private BindingResult result;

	@Mock
	private ApplicationDao dao;

	@Test
	public void testIfCreateWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.create(entity, result);

		// then
		verify(service, atLeastOnce()).create(entity);
	}

	@Test
	public void testIfCreateWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.create(entity, result);

		// then
		verify(service, times(0)).create(entity);
	}

	@Test
	public void testIfVerifyWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.VERIFIED).build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.verify(entity, result);

		// then
		verify(service, atLeastOnce()).verify(entity);
	}

	@Test
	public void testIfVerifyWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.verify(entity, result);

		// then
		verify(service, times(0)).verify(entity);
	}

	@Test
	public void testIfAcceptWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.ACCEPTED).build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.accept(entity, result);

		// then
		verify(service, atLeastOnce()).accept(entity);
	}

	@Test
	public void testIfAcceptWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.accept(entity, result);

		// then
		verify(service, times(0)).accept(entity);
	}

	@Test
	public void testIfPublishWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.PUBLISHED).build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.publish(entity, result);

		// then
		verify(service, atLeastOnce()).publish(entity);
	}

	@Test
	public void testIfPublishWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.publish(entity, result);

		// then
		verify(service, times(0)).publish(entity);
	}

	@Test
	public void testIfRejectWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.REJECTED).build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.reject(entity, result);

		// then
		verify(service, atLeastOnce()).reject(entity);
	}

	@Test
	public void testIfRejectWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.reject(entity, result);

		// then
		verify(service, times(0)).reject(entity);
	}

	@Test
	public void testIfDeleteWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.DELETED).withDeletionCause("ABC").build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.delete(entity, result);

		// then
		verify(service, atLeastOnce()).delete(entity);
	}

	@Test
	public void testIfDeleteWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.delete(entity, result);

		// then
		verify(service, times(0)).delete(entity);
	}

	@Test
	public void testIfUpdateWillCallServiceOnValidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.CREATED).build();
		when(result.hasErrors()).thenReturn(false);

		// when
		testee.update(entity, result);

		// then
		verify(service, atLeastOnce()).update(entity);
	}

	@Test
	public void testIfUpdateWillNotCallServiceOnInvalidEntity() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withName(null).withDescription(null).withStatus(ApplicationStatus.DELETED).build();
		when(result.hasErrors()).thenReturn(true);

		// when
		testee.update(entity, result);

		// then
		verify(service, times(0)).update(entity);
	}

	@Test
	public void testList() {
		// given
		List<ApplicationEntity> entityList = generateData();
		when(service.list(anyString(), any(ApplicationStatus.class), any())).thenReturn(entityList);

		// when
		List<ApplicationEntity> result = testee.list("abc", ApplicationStatus.ACCEPTED, null);

		// then
		assertThat(result).isNotNull();
		assertThat(result.size()).isGreaterThan(0);
	}

	private List<ApplicationEntity> generateData() {
		List<ApplicationEntity> entities = new ArrayList<>();
		ApplicationStatus[] statuses = ApplicationStatus.values();
		for(int i = 0; i < 100; i++) {
			ApplicationEntity entity = new ApplicationEntity();
			entity.setName("Entity"+(i+1)+" "+((char)i+1));
			entity.setDescription("Description");
			entity.setStatus(statuses[i % statuses.length]);
			entities.add(entity);
		}
		return entities;
	}

}
