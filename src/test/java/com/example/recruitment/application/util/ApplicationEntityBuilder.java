package com.example.recruitment.application.util;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;

/**
 * Created by t0meck on 29.05.2016.
 */
public class ApplicationEntityBuilder {
	private ApplicationEntity entity;

	public ApplicationEntityBuilder() {
		entity = new ApplicationEntity();
	}

	public ApplicationEntityBuilder buildDefault() {
		entity.setName("Sample application");
		entity.setDescription("Sample description");
		entity.setStatus(ApplicationStatus.CREATED);
		return this;
	}

	public ApplicationEntityBuilder withName(String name) {
		entity.setName(name);
		return this;
	}

	public ApplicationEntityBuilder withDescription(String description) {
		entity.setDescription(description);
		return this;
	}

	public ApplicationEntityBuilder withStatus(ApplicationStatus status) {
		entity.clearStatus();
		entity.setStatus(status);
		return this;
	}

	public ApplicationEntityBuilder withNumber(Long number) {
		entity.setNumber(number);
		return this;
	}

	public ApplicationEntityBuilder withDeletionCause(String cause) {
		entity.setDeletionCause(cause);
		return this;
	}

	public ApplicationEntityBuilder withId(Long id) {
		entity.setId(id);
		return this;
	}

	public ApplicationEntity build() {
		return this.entity;
	}

}
