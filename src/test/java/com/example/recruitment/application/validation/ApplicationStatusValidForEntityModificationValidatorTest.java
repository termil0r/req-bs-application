package com.example.recruitment.application.validation;

import com.example.recruitment.application.dal.ApplicationDao;
import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.example.recruitment.application.util.ApplicationEntityBuilder;
import org.javers.core.Javers;
import org.javers.core.diff.Diff;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by t0meck on 30.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationStatusValidForEntityModificationValidatorTest {

	@InjectMocks
	private ApplicationStatusValidForEntityModificationValidator testee = new ApplicationStatusValidForEntityModificationValidator();

	@Mock
	private ApplicationDao dao;

	@Mock
	private Javers javers;

	@Mock
	private Diff diffResult;

	@Test
	public void testValidIfNull() {
		// given

		// when
		boolean result = testee.isValid(null, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfNotPersisted() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withId(null).build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfNoOldVersion() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withId(1L).build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfOldVersionStatusCreatedAndSameAsNewVersion() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.CREATED).build();
		when(dao.findOne(entity.getId())).thenReturn(entity);
		when(javers.compare(entity, entity)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfOldVersionStatusVerifiedAndSameAsNewVersion() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.VERIFIED).build();
		when(dao.findOne(entity.getId())).thenReturn(entity);
		when(javers.compare(entity, entity)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfOldValueStatusCreatedValidAndMissmatchNewValueStatusVerified() {
		// given
		ApplicationEntity oldValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.CREATED).build();
		ApplicationEntity newValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.VERIFIED).build();

		when(dao.findOne(newValue.getId())).thenReturn(oldValue);
		when(javers.compare(oldValue, newValue)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);


		// when
		boolean result = testee.isValid(newValue, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfOldValueStatusVerifiedValidAndMissmatchNewValueStatusCreated() {
		// given
		ApplicationEntity oldValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.VERIFIED).build();
		ApplicationEntity newValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(ApplicationStatus.CREATED).build();

		when(dao.findOne(newValue.getId())).thenReturn(oldValue);
		when(javers.compare(oldValue, newValue)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);


		// when
		boolean result = testee.isValid(newValue, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidAllCombinationOfInvalidStatuses() {
		// given
		List<ApplicationStatus> validStatuses = Arrays.asList(ApplicationStatus.CREATED, ApplicationStatus.VERIFIED);
		List<ApplicationStatus> invalidStatuses = Arrays.asList(ApplicationStatus.values()).stream().filter(s -> !validStatuses.contains(s)).collect(Collectors.toList());
		ApplicationEntity oldValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		ApplicationEntity newValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		when(dao.findOne(newValue.getId())).thenReturn(oldValue);
		when(javers.compare(oldValue, newValue)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);


		// when
		for(ApplicationStatus validStatus : validStatuses) {
			for(ApplicationStatus invalidStatus : invalidStatuses) {
				oldValue.clearStatus();
				newValue.clearStatus();
				oldValue.setStatus(validStatus);
				newValue.setStatus(invalidStatus);

				boolean result = testee.isValid(newValue, null);
				assertThat(result).isTrue();

				oldValue.clearStatus();
				newValue.clearStatus();
				oldValue.setStatus(invalidStatus);
				newValue.setStatus(validStatus);

				result = testee.isValid(newValue, null);
				assertThat(result).isTrue();
			}
		}

		for(ApplicationStatus invalidStatus1 : invalidStatuses) {
			for(ApplicationStatus invalidStatus2 : invalidStatuses) {
				oldValue.clearStatus();
				newValue.clearStatus();
				oldValue.setStatus(invalidStatus1);
				newValue.setStatus(invalidStatus2);

				boolean result = testee.isValid(newValue, null);
				assertThat(result).isTrue();
			}
		}
	}

	@Test
	@Ignore
	public void testInValidAllCombinationOfInValidStatuses() {
		// given
		List<ApplicationStatus> validStatuses = Arrays.asList(ApplicationStatus.CREATED, ApplicationStatus.VERIFIED);
		List<ApplicationStatus> invalidStatuses = Arrays.asList(ApplicationStatus.values()).stream().filter(s -> !validStatuses.contains(s)).collect(Collectors.toList());
		ApplicationEntity oldValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		ApplicationEntity newValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		when(dao.findOne(newValue.getId())).thenReturn(oldValue);
		when(javers.compare(oldValue, newValue)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(true);

		// when
		for(ApplicationStatus invalidStatus1 : invalidStatuses) {
			for(ApplicationStatus invalidStatus2 : invalidStatuses) {
				if (invalidStatus1 == invalidStatus2) {
					continue;
				}

				oldValue.clearStatus();
				newValue.clearStatus();
				oldValue.setStatus(invalidStatus1);
				newValue.setStatus(invalidStatus2);

				boolean result = testee.isValid(newValue, null);
				assertThat(result).isFalse();
			}
		}
	}

	@Test
	public void testValidAllCombinationOfValidStatuses() {
		// given
		List<ApplicationStatus> validStatuses = Arrays.asList(ApplicationStatus.CREATED, ApplicationStatus.VERIFIED);
		List<ApplicationStatus> invalidStatuses = Arrays.asList(ApplicationStatus.values()).stream().filter(s -> !validStatuses.contains(s)).collect(Collectors.toList());
		ApplicationEntity oldValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		ApplicationEntity newValue = new ApplicationEntityBuilder().buildDefault().withId(1L).withStatus(null).build();
		when(dao.findOne(newValue.getId())).thenReturn(oldValue);
		when(javers.compare(oldValue, newValue)).thenReturn(diffResult);
		when(diffResult.hasChanges()).thenReturn(false);

		// when
		for(ApplicationStatus invalidStatus1 : validStatuses) {
			for(ApplicationStatus invalidStatus2 : validStatuses) {
				oldValue.clearStatus();
				newValue.clearStatus();
				oldValue.setStatus(invalidStatus1);
				newValue.setStatus(invalidStatus2);

				boolean result = testee.isValid(newValue, null);
				assertThat(result).isTrue();
			}
		}
	}

	@Test
	public void testInitialization() {
		// given

		// when
		testee.initialize(null);

		// then
		// will not produce any exceptions/errors
	}
}
