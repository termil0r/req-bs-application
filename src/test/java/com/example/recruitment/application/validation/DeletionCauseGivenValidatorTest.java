package com.example.recruitment.application.validation;

import com.example.recruitment.application.model.ApplicationEntity;
import com.example.recruitment.application.model.ApplicationStatus;
import com.example.recruitment.application.util.ApplicationEntityBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.assertThat;

/**
 * Created by t0meck on 30.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DeletionCauseGivenValidatorTest {

	private DeletionCauseGivenValidator testee = new DeletionCauseGivenValidator();

	@Test
	public void testValidIfNull() {
		// given

		// when
		boolean result = testee.isValid(null, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testValidIfIncorrectStatus() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().build();
		List<ApplicationStatus> statuses = Arrays.asList(ApplicationStatus.values()).stream().filter(s -> s != ApplicationStatus.DELETED && s != ApplicationStatus.REJECTED).collect(Collectors.toList());

		// when
		for(ApplicationStatus status : statuses) {
			entity.clearStatus();
			entity.setStatus(status);

			boolean result = testee.isValid(entity, null);

			// then
			assertThat(result).isTrue();
		}
	}

	@Test
	public void testValidIfCorrectStatusAndCauseNotEmpty() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.DELETED).withDeletionCause("TEST").build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isTrue();
	}

	@Test
	public void testInValidIfCorrectStatusAndCauseEmpty() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.DELETED).withDeletionCause("").build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isFalse();
	}

	@Test
	public void testInValidIfCorrectStatusAndCauseBlank() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.DELETED).withDeletionCause(" ").build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isFalse();
	}

	@Test
	public void testInValidIfCorrectStatusAndCourseNull() {
		// given
		ApplicationEntity entity = new ApplicationEntityBuilder().buildDefault().withStatus(ApplicationStatus.DELETED).withDeletionCause(null).build();

		// when
		boolean result = testee.isValid(entity, null);

		// then
		assertThat(result).isFalse();
	}

	@Test
	public void testInitialization() {
		// given

		// when
		testee.initialize(null);

		// then
		// will not produce any exceptions/errors
	}
}
