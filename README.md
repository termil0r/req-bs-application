Repository containing example recrutation application in one of the companies.

Requirements:
- New application can only be save while giving name and description
- Application name/description can only change in CREATED or VERIFIED states
- While deleting/rejecting application it is required to give cause
- Full change history must be kept in application
- There should be possibility to search through applications with pagination (default = 10 rows) including
  by name and state
- At the time of publishing, document should be given unique numeric number/id
- There should be rest endpoints
- There shouldn't be gui
- Applications must be kept in SQL or NOSQL database.



As there's no history entity then how history is handled?:
As it was required that full changes history should be held in database, here it is done by Javers library.
At start time Javers will create special tables to store its data regarding changing objects.
One of this tables will contain each "snapshot" of entity whenever change will be made to it and persisted.

You can view database using H2 web console



What's been assumed:
- State transition from A to A is valid
- There's no limit on string input
- search function will search for: name, status, name & status
- Used database is H2 configured as in-memory database thus after closing container with this project, data will be lost
- In case of errors, exceptions are not caught. Thy're just passed as is.


There's a lot of place for improovment here but this application is just technology demo

